Website
=======
http://sourceforge.net/projects/ftgl/

License
=======
MIT license (see the file source/COPYING)

Version
=======
2.1.3-rc5

Source
======
ftgl-2.1.3-rc5.tar.gz (sha256: 5458d62122454869572d39f8aa85745fc05d5518001bcefa63bd6cbb8d26565b)

Requires
========
* FreeType